﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpCreateFile
{
    class Program
    {
        static void Main(string[] args)
        {
            var program = new Program();
            program.CreateFile1();
            program.CreateFile2();
            program.CreateFile3();
        }

        private void CreateFile1()
        {
            var path = "sample1.txt";
            var fileStream = File.Create(path);
            fileStream.Close();
        }

        private void CreateFile2()
        {
            var path = "sample2.txt";
            var streamWriter = File.CreateText(path);
            streamWriter.Close();
        }

        private void CreateFile3()
        {
            var path = "sample3.txt";
            var streamWriter = new StreamWriter(path);
            streamWriter.Close();
        }
    }
}
